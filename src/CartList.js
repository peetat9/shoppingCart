import React,{Component} from 'react'
import CartItem from './CartItem'

class CartList extends Component {
    constructor(){
        super()
        this.state = {
           
        }
    }

    render(){
        let {items,cart,onDelete,onIncrease,onDecrease}=this.props
        var a=[]
        console.log(cart)
        for(let i in cart){
            a.push(<CartItem nameItem={i} unit={cart[i].unit} onDeleteItem={onDelete} onIn={onIncrease} onDe={onDecrease}/>)
        }
        // console.log(items)
        return (
            <div className='CartList'>
                <ul>
                    {a}
                </ul>
            </div>
        )
    }
}
export default CartList