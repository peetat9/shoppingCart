import React,{Component} from 'react'
class CartItem extends Component{
    render(){
        let {nameItem,unit,onDeleteItem,onIn,onDe} = this.props

        return (
            <li className='CartItem'>
                {nameItem}  <input type='text' size='2' value={parseInt(unit)}/><button onClick={()=>{onDe(nameItem)}}>-</button><button onClick={()=>{onIn(nameItem)}}>+</button><button onClick={()=>{onDeleteItem(nameItem)}}>Delete</button>
            </li>
        )
    }
}
export default CartItem