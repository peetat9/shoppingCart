import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
// import { Navbar, Jumbotron, Button } from 'react-bootstrap'
import CartTotal from './CartTotal'
import CartList from './CartList'
import CartCheckout from './CartCheckout'
import Menu from './Menu'
import im1 from './fastF/1.jpeg'
import im2 from './fastF/2.jpeg'
import im3 from './fastF/3.jpeg'
import im4 from './fastF/4.jpeg'

class App extends Component {
  
  constructor(){
    super()
    this.state = {
      Cart:{}
    }
  }
  increaseUnit(key){
    //+
    let jsonCart=this.state.Cart
    jsonCart[key].unit+=1
    this.setState({
      Cart:jsonCart
    })
  }

  decreaseUnit(key){
    //-
    let jsonCart=this.state.Cart
    if(jsonCart[key].unit <= 1){
      delete jsonCart[key]
    }else{
      jsonCart[key].unit-=1
    }
    
    this.setState({
      Cart:jsonCart
    })
  }

  addCart(newItem,price){
    let jsonCart=this.state.Cart
    if(jsonCart[newItem]){
      jsonCart[newItem].unit+=1
    }else{
      jsonCart[newItem]={'price':price,unit:1}
    }
    
    this.setState({
      Cart:jsonCart
    })
    console.log('state cart : '+JSON.stringify(this.state.Cart))
  }

  deleteCart(key){
    console.log('key Delete : '+key)
    let jsonCart=this.state.Cart
    console.log('json : '+JSON.stringify(jsonCart))
    delete jsonCart[key]
    console.log('did json : '+JSON.stringify(jsonCart))

    this.setState({
      Cart:jsonCart
    })
  }

  getPrice(){
    let tPrice = 0
    for(let i in this.state.Cart){
      tPrice+=this.state.Cart[i].price*this.state.Cart[i].unit
    }
    return tPrice
  }

  checkOut(){
    fetch('http://localhost:4000/checkOut', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.state.Cart)
    }).then(response => response.json())
    .then(jsondata => console.log(jsondata))
  }

  render() {
    return (
      <div className='App'>
        <header className='App-header'>
          <img src={logo} className='App-logo' alt='logo' />
          <h3 className='App-title'>Welcome to Restaurant</h3>
        </header>
        <div className='row'>
          <div className='col-xs-4'>
            Cart
            <CartList items={this.state.CartItem} cart={this.state.Cart} onDelete={this.deleteCart.bind(this)} onIncrease={this.increaseUnit.bind(this)} onDecrease={this.decreaseUnit.bind(this)}/>
            <CartTotal price={this.getPrice()}/>
            <button onClick={()=>{this.checkOut()}}>Checkout</button>
            {/* <CartCheckout /> */}
          </div>
          <div className='col-xs-8'>Menu
            <div className='row'>
              <div className='col-xs-3'>
                <Menu name='food1' price='10' nameImg={im1} onAddCart = {this.addCart.bind(this)}/>
              </div>
              <div className='col-xs-3'>
                <Menu name='food2' price='20' nameImg={im2} onAddCart = {this.addCart.bind(this)}/>
              </div>
              <div className='col-xs-3'>
                <Menu name='food3' price='30' nameImg={im3} onAddCart = {this.addCart.bind(this)}/>
              </div>
              <div className='col-xs-3'>
                <Menu name='food4' price='40' nameImg={im4} onAddCart = {this.addCart.bind(this)}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
