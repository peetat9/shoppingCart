import React , {Component} from 'react'
import im from './fastF/1.jpeg'
class Menu extends Component{
    render(){
        let {nameImg,name,price,onAddCart} = this.props
        return (

            <div className='Menu'>
                <img src={nameImg} width="100" height="100"/><br/>
                {name}<br/>
                {price}Baht<br/>
                <button onClick={()=>{onAddCart(name,price)}}>Add to cart</button>
            </div>
        )
    }
}
export default Menu